package threesolid;

public class SuperWorker implements IWorkerWork, IWorkerEat{
    public void work() {
        //.... working much more
    }

    public void eat() {
        //.... eating in launch break
    }

}
