package threesolid;

public class Worker  implements IWorkerWork, IWorkerEat{
  public void work() {
      // ....working
  }

  public void eat() {
      //.... eating in lunch break
  }
}