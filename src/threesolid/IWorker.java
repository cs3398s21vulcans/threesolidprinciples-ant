package threesolid;

public interface IWorker extends IWorkerEat, IWorkerWork {
}
    interface IWorkerEat {
            public void eat();
        }

    interface IWorkerWork {
            public void work();
    }

