# README #

##  Vulcans Assignment 9 ##

### Reasoning for Reorganizing the Code ###

* Reorginized code to meet the SOLID Principles
* Seperated the interface IWorker into two seperate interface files (IWorkerEat & IWorkerWork) to fit the Interface Segregation Principle
* Created robot in it's own Java file and moved classes Worker and SuperWorker on their own to fit the Single Responsibilty Principle
* Interfaces:
    - IWorkerWork & IWorkerEat are the interfaces, so they are "segregated" into their own files (for easy implementation)
* Single Responsibility
    - Robot: Does robot stuff (works)
    - Worker & SuperWorker: Does worker stuff (works and eats)
    - Manager: Manages workers
    - Interfaces are abstract
    - Main file is separate from all others
* Open/Close
    - Each function does exactly what the function's name says, and nothing else.
    
###  Principles used  ###
* Interface Segregation Principle
* Single Responsibilty Principle

### How to Run ###
